{{-- @php
    $role = Auth::user()->role;
@endphp --}}

<ul class="nav pcoded-inner-navbar">
    {{-- @if ( $role == 'admin' || $role == 'calonsiswa' || $role == 'kepsek') --}}
        <li class="nav-item {{ Request::path() == 'dashboard' ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-airplay"></i></span><span class="pcoded-mtext">Program</span></a>
        </li>
    {{-- @endif --}}
</ul>
