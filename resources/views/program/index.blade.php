@extends('template_admin.template')
@section('konten')
    <style>
        .js-example-basic-single {
            display: block !important;
            height: 50px !important;
            white-space: nowrap !important;
            line-height: 26px !important;
        }
    </style>
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">{{ $main_title }}</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-file-text"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="javascript:">{{ $main_title }}</a></li>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- [ Main Content ] start -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-lg-4 mt-3">
                                            <h5>{{ $main_title }}</h5>
                                        </div>
                                        <div class="col-lg-8 text-right">
                                            <a href="#" onclick="dialogModal()" class="btn btn-primary btn-lg">
                                                <span class="fa fa-plus" aria-hidden="true"></span> Tambah Data
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @include('template_admin.notifikasi')
                                <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="datatable-wrap my-3 p-2">
                                        <table class=" table table-striped table-bordered dt-responsive nowrap data-table"
                                            data-auto-responsive="false" id="DataTables_Table_1"
                                            aria-describedby="DataTables_Table_1_info">
                                            <thead>
                                                <tr class="">

                                                    <th class="" width="10%"><span>No.</span></th>
                                                    <th class="" width="30%"><span>Sumber Dana</span></th>
                                                    <th class="" width="20%"><span>Program</span></th>
                                                    <th class="" width="30%"><span>Keterangan</span></th>
                                                    <th class="" width="30%"><span>Created At</span></th>
                                                    <th class="" width="5%"><span>Action</span></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ Main Content ] end -->
            </div>
        </div>
    </div>
    </div>

    <div class="modal" method="POST" id="dialogModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="myForm" name="myForm">
                    <div class="modal-body">
                        <div class="" id="errorMessages"></div>
                        <input type="hidden" id="id" name="id">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Sumber Dana<span class="text-danger">*</span> </label>
                                <input type="text" class="form-control" name="sumber_dana" id="sumber_dana">
                            </div>
                            <div class="col-md-6">
                                <label for="">Program<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="program" id="program">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" class="form-control" id="keterangan" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push('custom-scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"
            integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('dashboard') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            className: "text-center",
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'sumber_dana',
                            name: 'sumber_dana',
                            className: "w260"
                        },
                        {
                            data: 'program',
                            name: 'program',
                            className: "w260"
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan',
                            className: "w260"
                        },
                        {
                            data: 'tanggal',
                            name: 'tanggal',
                            className: "w260"
                        },
                        {
                            data: 'action',
                            name: 'action',
                            className: "text-center",
                            orderable: false,
                            searchable: false,
                        },
                    ]
                });

                // add data
                $('#myForm').submit(function(event) {
                    event.preventDefault(); // Prevent the default form submission

                    // Get form values
                    const sumberDana = $('#sumber_dana').val();
                    const program = $('#program').val();

                    // Initialize an error message container
                    let errorMessages = [];

                    // Validate required fields
                    if (!sumberDana) {
                        errorMessages.push('<span class="text-danger">Sumber Dana is required.<span><br>');
                    }
                    if (!program) {
                        errorMessages.push('<span class="text-danger">Program is required.<span><br>');
                    }

                    // Display error messages if any
                    const errorMessagesDiv = $('#errorMessages');
                    errorMessagesDiv.html(); // Clear previous messages
                    if (errorMessages.length > 0) {

                        errorMessagesDiv.html('<div class="alert alert-danger" role="alert">' + errorMessages +
                            '</div>');
                        return; // Stop form submission if there are errors
                    } else {
                        errorMessagesDiv.html('');

                    }

                    // Serialize form data
                    // Serialize form data to JSON
                    const formData = {
                        id          : $('#id').val(),
                        sumber_dana : $('#sumber_dana').val(),
                        program     : $('#program').val(),
                        keterangan  : $('#keterangan').val()
                    };
                    console.log(formData)
                    $('#dialogModal').modal('hide')
                    // Send data using Axios
                    axios.post('store', formData, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                        .then(response => {
                            Swal.fire({
                                title: "Sukses",
                                text: response.data.message,
                                icon: "success"
                            }).then(function(response) {
                                // console.log(datatables)
                                table.ajax.reload();
                                // window.location.reload();
                            });
                            // Handle success (e.g., show a success message or redirect)
                        })
                        .catch(error => {
                            Swal.fire({
                                title: "Gagal",
                                text: response.data.message,
                                icon: "error"
                            }).then(function(response) {
                                table.ajax.reload();
                                // window.location.reload();
                            });
                            console.error('Error:', error);
                            // Handle error (e.g., show an error message)
                        })
                });

                function hapus(data){
                    Swal.fire({
                        title: "Hapus data ini?",
                        text: "data yang dihapus tidak dapat dikembalikan!",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#d33",
                        cancelButtonColor: "#6c757d",
                        confirmButtonText: "Ya",
                        cancelButtonText: "Tidak",
                        revertButton: true,
                        showLoaderOnConfirm: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            axios.get(`destroy/`+data)
                                .then(function(response) {
                                    Swal.fire({
                                        title: "Terhapus!",
                                        text: "Sukses Menghapus Data",
                                        icon: "success"
                                    }).then(function(response) {
                                        table.ajax.reload();
                                    });
                                })
                                .catch(function(error) {
                                    // handle error
                                    Swal.fire({
                                        title: "Terhapus!",
                                        text: "Gagal menghapus data ",
                                        icon: "error"
                                    }).then(function(response) {
                                        table.ajax.reload();
                                    });
                                })

                        }
                    });
                };

                function edit(data){

                    // fetch data
                    axios.get('edit/'+data).then(function(response) {

                        var result = response.data.data;
                        $('#id').val(result.id)
                        $('#sumber_dana').val(result.sumber_dana)
                        $('#program').val(result.program)
                        $('#keterangan').val(result.keterangan)

                        // alert(data)
                        $('#dialogModal').modal('show')

                    }).catch(function(error) {
                        // handle error
                        Swal.fire({
                            title: "Error!",
                            text: "Gagal mengambil data",
                            icon: "error"
                        }).then(function(response) {
                            table.ajax.reload();
                        });
                    })
                };

                $('.data-table tbody').on('click', 'button.hapus', function() {
                    var data = table.row($(this).parents('tr')).data();
                    hapus(data.id);
                });

                $('.data-table tbody').on('click', 'button.edit', function() {
                    var data = table.row($(this).parents('tr')).data();
                    edit(data.id);
                });

            });

        </script>

        <script>
            function dialogModal() {
                $('#id').val('')
                $('#sumber_dana').val('')
                $('#program').val('')
                $('#keterangan').val('')
                $('#dialogModal').modal('show')
            }
        </script>
    @endpush
@endsection
