<div class="main-menu d-none d-md-block">
    <nav>
        <ul id="navigation">
            <li><a href="{{ ('/') }}">Beranda</a></li>
            <li><a href="#">Profil Desa</a>
                <ul class="submenu">
                    <li><a  href="{{ ('/visi_misi') }}">Sejarah</a></li>
                    <li><a  href="{{ ('/visi_misi') }}">Visi & Misi</a></li>
                    <li><a href="{{ ('/perangkat_desa') }}">Perangkat Desa</a></li>
                    <li><a href="{{ ('/demografis') }}">Demografi Penduduk</a></li>
                    <li><a href="{{ ('/fasilitas_umum') }}">Fasilitas Umum</a></li>
                    <li><a href="{{ ('/anggaran_dana') }}">Anggaran Desa</a></li>
                </ul>
            </li>
            <li><a href="{{ ('/potensi_desa') }}">Potensi Desa</a></li>
            <li><a href="#">Informasi Pendidikan</a>
                <ul class="submenu">
                    <li><a href="{{ ('/paud') }}">PAUD</a></li>
                    <li><a href="{{ ('/tk') }}">TK</a></li>
                    <li><a href="{{ ('/sd_mi') }}">SD / MI</a></li>
                    <li><a href="{{ ('/smp_mts') }}">SMP / MTS</a></li>
                    <li><a href="{{ ('/sma_smk_ma') }}">SMA / SMK / MA</a></li>
                </ul>
            </li>
            <li><a href="#">Informasi</a>
                <ul class="submenu">
                    <li><a href="{{ ('/berita') }}">Berita</a></li>
                    <li><a href="blog.html">Mekanisme Pengurusan Berkas</a></li>
                    <li><a href="{{ ('/layanan_pengaduan') }}">Layanan Pengaduan (Saran Kritik)</a></li>
                </ul>
            </li>
            <li><a href="#">Galeri</a>
                <ul class="submenu">
                    <li><a href="{{ ('/foto_desa') }}">Foto & Video Desa</a></li>
                    <li><a href="{{ ('/foto_kegiatan') }}">Foto & Video Kegiatan</a></li>
                </ul>
            </li>
            <li><a href="#">Link Terkait</a>
                <ul class="submenu">
                    <li><a href="elements.html">BPD</a></li>
                    <li><a href="blog.html">BUMD</a></li>
                    <li><a href="blog.html">PKK</a></li>
                    <li><a href="blog.html">Badan Keswadayaan Masyarakat</a></li>
                    <li><a href="blog.html">Perlindungan Masyarakat</a></li>
                    <li><a href="blog.html">Kader Pemberdayaan Masyarakat Desa</a></li>
                    <li><a href="blog.html">Pos Pelayanan Terpadu</a></li>
                    <li><a href="blog.html">Pos Pelayanan Terpadu Lansia</a></li>
                    <li><a href="blog.html">Pos Bindu</a></li>
                    <li><a href="blog.html">Pos Kesehatan Desa</a></li>
                    <li><a href="blog.html">Karang Taruna</a></li>
                </ul>
            </li>
            <li><a href="{{ ('/login') }}">Login</a></li>
        </ul>
    </nav>
</div>
