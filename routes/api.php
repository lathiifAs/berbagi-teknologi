<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\API\AgendaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
    Route::get('/cek-user', [AuthController::class, 'cekUser']);
    Route::get('/siswa-profile', [AuthController::class, 'siswaProfile']);
    Route::get('/get-siswa', [AuthController::class, 'getSiswa']);
    Route::get('/fetch-pengajar', [AuthController::class, 'fetchPengajar']);

    // Agenda
    Route::get('/agenda', [AgendaController::class, 'index']);
    Route::post('/add-agenda', [AgendaController::class, 'store']);
    Route::get('/update-st-agenda', [AgendaController::class, 'updateAgenda']);
    Route::get('/riwayat', [AgendaController::class, 'riwayat']);

});
