<?php

use App\Http\Controllers\ProgramController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProgramController::class, 'index'])->name('dashboard');
Route::post('store', [ProgramController::class, 'store'])->name('store');
Route::get('destroy/{id}', [ProgramController::class, 'destroy'])->name('destroy');
Route::get('edit/{id}', [ProgramController::class, 'edit'])->name('edit');

// Route::get('/', [ProgramController::class, 'index'])->name('dashboard');


// Route::get('/get-monitoring',  [AdminController::class, 'getMonitoring'])->name('get-monitoring');
// Route::get('/get-agenda-kelas/{id}',  [AdminController::class, 'getAgendakelas'])->name('get-agenda-kelas');
// Route::get('/get-agenda-kelas-detail/{id}',  [AdminController::class, 'getAgendakelasdetail'])->name('get-agenda-kelas-detail');

// // rekap
// Route::post('/rekap/guru', [AdminController::class, 'rekapGuru'])->name('rekap/guru');
// Route::post('/rekap/siswa', [AdminController::class, 'rekapSiswa'])->name('rekap/siswa');

// PKL
// Route::get('/set-pkl/{kelas_id}', [AdminController::class, 'setPkl'])->name('set-pkl');
// Master Data


// Route::get('/register-calon',  [RegisterController::class, 'showRegisterForm'])->name('register-calon');
// Route::post('/register-calon',  [RegisterController::class, 'registerStore'])->name('register-calon');

// Route::group(['middleware'=>'role:kepsek,calonsiswa'],function () {
//     /* beberapa route di dalam group */
//     Route::group([
//         'middleware' => ['userverify'] // 'throttle:10,2'
//     ], function () {
//         Route::get('siswa-diterima',   [DataSiswaController::class, 'siswaDiterima'])->name('siswa-diterima');
//         Route::get('siswa-ditolak',   [DataSiswaController::class, 'siswaDitolak'])->name('siswa-ditolak');
//     });
// });

// Route::group(['middleware'=>'role:kepsek,calonsiswa,admin'],function () {
//     /* beberapa route di dalam group */
//     Route::group([
//         'middleware' => ['userverify'] // 'throttle:10,2'
//     ], function () {
//         Route::get('/dashboard',   [AdminController::class, 'index'])->name('dashboard');
//         Route::get('/profile-sekolah',   [ProfileSekolahController::class, 'index'])->name('profile-sekolah');

//         Route::get('siswa-diterima',   [DataSiswaController::class, 'siswaDiterima'])->name('siswa-diterima');
//         Route::get('siswa-ditolak',   [DataSiswaController::class, 'siswaDitolak'])->name('siswa-ditolak');
//     });
// });

// Route::group(['middleware'=>'role:admin'],function () {
//     /* beberapa route di dalam group */
//         Route::get('verifikasi',   [VerifikasiController::class, 'index'])->name('verifikasi');
//         Route::get('set-verifikasi/{id}/{jenis}',   [VerifikasiController::class, 'verifikasi'])->name('set-verifikasi');

//         Route::get('data-siswa',   [DataSiswaController::class, 'index'])->name('data-siswa');
//         Route::get('change-status-penerimanaan/{id}/{status}',   [DataSiswaController::class, 'changeStatus'])->name('change-status-penerimanaan');
//         Route::post('update-siswa-diterima',   [DataSiswaController::class, 'changeStatusDiterima'])->name('update-siswa-diterima');


//         // Master data
//         Route::get('/master/kelas', [KelasController::class, 'index'])->name('kelas');
//         Route::get('master/kelas/add_kelas', [KelasController::class, 'add_kelas'])->name('add_kelas');
//         Route::post('master/kelas/inputDatakelas', [KelasController::class, 'inputDatakelas'])->name('inputDatakelas');
//         Route::get('master/kelas/edit_kelas/{id}', [KelasController::class, 'edit_kelas'])->name('edit_kelas');
//         Route::post('master/kelas/updateDatakelas', [KelasController::class, 'updateDatakelas'])->name('updateDatakelas');
//         Route::get('hapus-kelas/{id}', [KelasController::class, 'hapus_kelas'])->name('hapus-kelas');

//         //Guru
//         Route::get('/master/guru',  [GuruController::class, 'index'])->name('guru');
//         Route::get('/master/guru/add_guru',  [GuruController::class, 'Add_guru'])->name('Add_guru');
//         Route::post('/master/guru/inputDataguru',  [GuruController::class, 'inputDataguru'])->name('inputDataguru');
//         Route::get('/master/guru/edit_guru/{id}',  [GuruController::class, 'edit_guru'])->name('edit_guru');
//         Route::post('/master/guru/updateguru', [GuruController::class, 'updateguru'])->name('updateguru');
//         Route::get('/master/guru/hapusguru/{id}', [GuruController::class, 'hapusguru'])->name('hapusguru');
//         Route::post('/import-guru', [GuruController::class, 'importGuru'])->name('import-guru');

//         // master mapel
//         Route::get('/master/mapel', [MapelController::class, 'index'])->name('master/mapel');
//         Route::get('/master/mapel/add', [MapelController::class, 'add'])->name('master/mapel/add');
//         Route::post('/master/mapel/add',  [MapelController::class, 'add_process'])->name('master/mapel/add');
//         Route::get('/master/mapel/edit/{id}', [MapelController::class, 'edit'])->name('master/mapel/edit');
//         Route::post('/master/mapel/update', [MapelController::class, 'update'])->name('master/mapel/update');
//         Route::get('/master/mapel/delete/{id}', [MapelController::class, 'delete'])->name('master/mapel/delete');
//         Route::post('/import-mapel', [MapelController::class, 'importMapel'])->name('import-mapel');

//         // master ekskul
//         Route::get('/master/ekskul', [EkskulController::class, 'index'])->name('master/ekskul');
//         Route::get('/master/ekskul/add', [EkskulController::class, 'add'])->name('master/ekskul/add');
//         Route::post('/master/ekskul/add',  [EkskulController::class, 'add_process'])->name('master/ekskul/add');
//         Route::get('/master/ekskul/edit/{id}', [EkskulController::class, 'edit'])->name('master/ekskul/edit');
//         Route::post('/master/ekskul/update', [EkskulController::class, 'update'])->name('master/ekskul/update');
//         Route::get('/master/ekskul/delete/{id}', [EkskulController::class, 'delete'])->name('master/ekskul/delete');
//         Route::post('/import-ekskul', [EkskulController::class, 'importekskul'])->name('import-ekskul');

//         Route::get('setting',   [SettingController::class, 'index'])->name('setting');
//         Route::post('setting.update',   [SettingController::class, 'update'])->name('setting.update');

//         // Master data akun
//         Route::get('/master/akun',   [DataAkunController::class, 'index'])->name('master/akun');
//         Route::get('/master/akun/add', [DataAkunController::class, 'add'])->name('add_akun');
//         Route::get('/edit_akun/{id}', [DataAkunController::class, 'edit'])->name('edit_akun');
//         Route::get('/hapus-akun/{id}', [DataAkunController::class, 'hapus'])->name('hapus-akun');
//         Route::post('/master/guru/inputDataAkun',  [DataAkunController::class, 'store'])->name('inputDataAkun');
// });

// Route::group(['middleware'=>'role:calonsiswa'],function () {
//         Route::get('pendaftaran',   [PendaftaranController::class, 'index'])->name('pendaftaran');
//         Route::post('pendaftaran-update',   [PendaftaranController::class, 'update'])->name('pendaftaran-update');
//         Route::get('pengumuman',   [PengumumanController::class, 'index'])->name('pengumuman');
//         Route::get('pengumuman-list-ditolak',   [PengumumanController::class, 'indexListDitolak'])->name('pengumuman-list-ditolak');
// });

// Route::group(['middleware'=>'role:kepsek'],function () {
//     Route::get('laporan',   [LaporanController::class, 'index'])->name('laporan');
//     Route::get('laporan-list-ditolak',   [LaporanController::class, 'indexDitolak'])->name('laporan-list-ditolak');
// });

// Auth::routes(['verify' => true]);
// Auth::routes();

// // Route::get('/logout', 'Auth\LoginController@logout');
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

