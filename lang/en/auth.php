<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Akun tidak ditemukan atau tidak terdaftar.',
    'password' => 'Password salah.',
    'throttle' => 'Terlalu banyak mencoba login. coba setelah :seconds detik lagi.',

];
