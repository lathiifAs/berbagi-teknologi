<?php

namespace App\Http\Controllers;

use App\Models\Program;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;


class ProgramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $main_title = 'Program';
        if ($request->ajax()) {

            $data = Program::get();
            return DataTables::of($data)
                ->addColumn('action', function (Program $row) {
                    $html = '<div class="d-flex"><button type="button" class="btn btn-warning mb-0 p-2 edit"><span class="fa fa-edit"></span></button><button type="button" id="btnhapus" data-id="'.$row->id.'" class="btn btn-danger mb-0 p-2 hapus"><span class="fa fa-trash"></span></button></div>';
                    return $html;
                })
                ->addColumn('tanggal', function (Program $row) {
                    $html = Carbon::parse($row->created_at)->isoFormat('D MMMM Y');
                    return $html;
                })
                ->rawColumns(["action", "tanggal"])
                ->addIndexColumn()
                ->make(true);
        }

        return view('program.index', compact('main_title'));
    }

    public function store(Request $request)
    {
        $save = Program::updateOrCreate(
            [
                "id" => $request->id
            ],
            [
             "sumber_dana" => $request->sumber_dana,
             "program" => $request->program,
             "keterangan" => $request->keterangan,
            ]);
        if($save){
            return response([
                "success" => true,
                "message" => "Berhasil menyimpan data ",
                "data" => $request->all()
            ], 200);
        }else{
            return response([
                "success" => false,
                "message" => "Gagal menyimpan data !",
                "data" => $request->all()
            ], 400);
        }
    }

    function edit($id){
        try {
            $data = Program::where('id', $id)->first();
            if (!empty($data)) {
                return response([
                    "success" => true,
                    "message" => "Berhasil mengambil data ",
                    "data" => $data
                ], 200);
            } else {
                return response([
                    "success" => false,
                    "message" => "Gagal mengambil data !",
                ], 400);
            }
        } catch (\Throwable $th) {
            return response([
                "success" => false,
                "message" => "Gagal mengambil data !",
            ], 502);
        }
    }


    public function updateguru(Request $request)
    {
        $id = $request->id;
        $kolom = 'tidak boleh kosong';
        $validatedData = $request->validate(
            [
                'id' => 'required',
                'nama' => 'required',
                'nip' => 'required',
                'bidang' => 'required',
            ],
            [
                'id.required' => 'ID ' . $kolom,
                'nama.required' => 'Nama ' . $kolom,
                'bidang.required' => 'Bidang Keahlian ' . $kolom,
            ]
        );


        $send = Data_guru::where('id', $request->id)->update([

            'nama' => $request['nama'],
            'nip' => $request['nip'],
            'bidang_keahlian' => $request['bidang'],
        ]);

        if ($send) {
            return redirect()
                ->route('edit_guru', compact('id'))
                ->with([
                    'success' => 'Data berhasil diupdate'
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Ada kesalahan, coba lagi !'
                ]);
        }
    }

    public function destroy($id)
    {
        try {
            $hapus =Program::findOrFail($id);
            $hapus->delete();
            if ($hapus) {
                return response([
                    "success" => true,
                    "message" => "Berhasil menghapus data ",
                ], 200);
            } else {
                return response([
                    "success" => false,
                    "message" => "Gagal menghapus data !",
                ], 400);
            }
        } catch (\Throwable $th) {
            return response([
                "success" => false,
                "message" => "Gagal menghapus data !",
            ], 502);
        }

    }
}
